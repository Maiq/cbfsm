#pragma once
#include <functional>
#include "StateBase.h"


template<typename A ,typename B , typename C >
class State: public StateBase{
    public:
    State(const std::string& stateName, A jobCallback = [](){},B enterCallback = [](){}, C exitCallback = [](){}): 
        StateBase(stateName),
        m_jobCallback {jobCallback},
        m_enterCallback {enterCallback},
        m_exitCallback {exitCallback}
        {
        };
        
    template<typename...Args>
    void onJob(Args...args) const{
        m_jobCallback(args...);
    }
    
    template<typename...Args>
    void onEnter(Args...args) const{
        m_enterCallback(args...);
    }
    
    template<typename...Args>
    void onExit(Args...args) const{
        m_exitCallback(args...);
    }
    
    private:
    const A m_jobCallback;
    const B m_enterCallback;
    const C m_exitCallback;

};