#include <iostream>
#include <memory>
#include <thread>
#include <functional>
#include "State.h"
#include <vector>
#include <tuple>
#include "FSM.h"
#include "Transition.h"
#include <algorithm>

class Light{
    public:
    void turnOn(){m_enable = true; std::cout << "I'm on!"<< std::endl;};
    void turnOff(){m_enable = false; std::cout<< "I'm off"<< std::endl;};
    void set(bool enable){m_enable = enable; std::cout << "I'm " << (m_enable ? "on" : "off")<< std::endl;};
    void toggle(){m_enable ^= true; std::cout << "I'm " << (m_enable ? "on" : "off") << std::endl;};
    
    private:
    bool m_enable = false;
    
};

constexpr auto c0 = "L0 off";
constexpr auto c1 = "L1 on";
constexpr auto c2 = "L2 on";
constexpr auto c3 = "L3 on";

int main(){

    Light l1;
    Light l2;
    Light l3;

    Transition p1("p1");
    Transition p2("p2");
    Transition p3("blackOut");
    Transition p4("L1L2");
    
    FSM<State<>*,SuperState* > fsm(
        new State<>(c0,[&l1,&l2,&l3](){l1.turnOff();l2.turnOff(),l3.turnOff();}),
        new SuperState("test",
        new State<>(c1),
        new State<>(c2,[&l1,&l2](){l1.turnOff();l2.turnOn();}),
        new State<>(c3,[&l1,&l3](){l1.turnOff();l3.turnOn();}),
        new State<std::function<void(bool)>>("L2onWithSet",[&l1,&l2](bool x_set){std::cout << "Argument" << std::endl;l1.turnOff();l2.set(x_set);})
        )
        );

    fsm.addTransition(p1,fsm[c0],fsm[c1]);
    fsm.addTransition(p2,fsm[c0],fsm[c1]);
    fsm.addTransition(p1,fsm[c1],fsm[c2]);
    fsm.addTransition(p2,fsm[c1],fsm[c3]);
    fsm.addTransition(p1,fsm[c2],fsm[c0]);
    fsm.addTransition(p2,fsm[c2],fsm[c0]);
    fsm.addTransition(p1,fsm[c3],fsm[c0]);
    fsm.addTransition(p2,fsm[c3],fsm[c0]);
    fsm.addTransition(p3,fsm["test"],fsm[c0]);
    fsm.addTransition(p4,fsm[c1],fsm["L2onWithSet"]);
    

    char userInput = '0';
    while( userInput != 'x'){
        std::cin >> userInput;
        std::cout << userInput << std::endl;
        if(userInput == '1'){
            fsm.process(p1);
        }
        if(userInput == '2'){
           fsm.process(p2);
        }
        if(userInput == '9'){
           fsm.process(p3);
        }
        if(userInput == '4'){
            fsm.process(p4,true);
        }
    }



}

