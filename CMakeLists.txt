cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(cbFSM VERSION 1.0)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)
# set(CMAKE_CXX_FLAGS "-O2 -Wall")

include_directories("${PROJECT_BINARY_DIR}")
file(GLOB SOURCES "${PROJECT_BINARY_DIR}/../*.cpp")

add_executable(cbFSM "${SOURCES}")