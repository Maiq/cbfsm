#pragma once
#include <tuple>
#include <map>
#include "Transition.h"
#include <vector>


template<typename...S>
std::map<std::string, StateBase*> makeEnumerationMap(S&&...states){
    std::map<std::string,StateBase*> enumMap;
    makeEnumerationMap(enumMap,std::forward<S>(states)...);
    return enumMap;
}

template<typename S>
void makeEnumerationMap(std::map<std::string,StateBase*>& m, S&& state){
    m.emplace(std::make_pair<std::string,StateBase*>(state->getName(),std::forward<StateBase*>(state)));
    for(auto substate : state->m_substates){
        m.emplace(std::make_pair<std::string,StateBase*>(substate->getName(),std::forward<StateBase*>(substate)));
    }
    
}

template<typename S, typename...Ss>
void makeEnumerationMap(std::map<std::string,StateBase*>& m, S&& state, Ss&&...states){
    m.emplace(std::make_pair<std::string,StateBase*>(state->getName(),std::forward<StateBase*>(state)));
    for(auto substate : state->m_substates){
        m.emplace(std::make_pair<std::string,StateBase*>(substate->getName(),std::forward<StateBase*>(substate)));
    }
    makeEnumerationMap(m,std::forward<Ss>(states)...);
}


template<typename...S>
class FSM{
    public:
    FSM(S...states): 
    m_map { makeEnumerationMap(states...) }
    {
        // if(m_map.size() != sizeof...(S)){
        //     throw(std::runtime_error("FSM Constructor - state duplicate?"));
        // }
        m_activeState = m_map.begin()->second;
        std::cout<<"ACTIVE IS " << m_activeState->getName() << std::endl;
        for(auto x : m_map){
            std::cout << x.first << " : " << x.second->getName() << std::endl;
        }
    }
    
    void addTransition(Transition Tr, StateBase* origin, StateBase* destination){
        //TODO: Ensure no duplicates
        m_transitionMap[Tr].emplace(std::make_pair(origin,destination));
    }

    template<typename...Args>
    void process(Transition Tr,Args...args){
        try{
            auto destination = m_transitionMap.at(Tr).at(m_activeState);        
            std::cout << "Going From " << m_activeState->getName() << " to " << destination->getName() << std::endl;
            m_activeState = destination;
            while(m_activeState->m_substates.size() != 0){
                m_activeState = m_activeState->m_substates[0];
            }
            m_activeState->fire(args...);
        }
        catch (std::out_of_range){
            if(m_activeState->m_parent != nullptr){
                std::cout << "Active: " << m_activeState->getName() << std::endl;
                std::cout << "Parent: " << m_activeState->m_parent->getName() << std::endl;
                m_activeState = m_activeState->m_parent;
                process(Tr,args...);
            }
        }
        
    } 

    

    StateBase* m_activeState;
    const std::map<std::string,StateBase*> m_map;
    
    std::map<Transition,std::map<StateBase*,StateBase*> > m_transitionMap;
    StateBase* operator [](std::string name) const {return m_map.at(name);};
    

};