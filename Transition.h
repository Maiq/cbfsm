#pragma once
#include "StateBase.h"

class Transition{
    public:
    Transition(std::string name): m_name { name } {};

    const std::string m_name;
    const bool operator < (const Transition &r) const{ return m_name < r.m_name;};
};