#pragma once
#include <string>
#include <functional>
#include <vector>

template<typename A = std::function<void()>,typename B = std::function<void()>, typename C = std::function<void()>>
class State;

class StateBase{
    public:
    
    StateBase(std::string name): m_stateName{ name } {};
    template<typename ...Args>
    StateBase(std::string name,Args...args): m_stateName{ name }, m_substates{args...} 
    {
        for(auto s : m_substates){
            s->m_parent = this;
        }
    };


    std::string getName() const{
    return m_stateName;
    }

    template<typename ...P> 
    void fire(P...args)
    {
            (static_cast< State<std::function<void(P...)> >* > (this))->onJob(args...);
    };

    StateBase* m_parent = nullptr;
    std::vector<StateBase*> m_substates; //MAYBE USEFUL

    protected:
    const std::string m_stateName;
};

using SuperState = StateBase;